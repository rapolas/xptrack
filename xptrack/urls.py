from django.conf.urls import patterns, include, url

urlpatterns = patterns(
    '',
    # Examples:
    url(r'^$', 'whiteboard.views.homepage', name='homepage'),
    url(r'^whiteboard/', include('whiteboard.urls')),
)
