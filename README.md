[![Build Status](https://drone.io/bitbucket.org/rapolas/xptrack/status.png)](https://drone.io/bitbucket.org/rapolas/xptrack/latest)

# README #

Simple story/task tracker.

### How do I get set up? ###

*Built and tested with python3.4 and django1.7b4, though in theory it should work under any python supported by django*

* git clone https://bitbucket.org/rapolas/xptrack.git
* pip install -r requirements.txt
* python manage.py migrate (this will crate db file one directory higher than a project)
* python manage.py test
* python manage.py runserver
* [visit localhost url](http://localhost:8000/)

### Author ###

* Rapolas K.

### Additional ###

* [Markdown cheatsheet](https://bitbucket.org/tutorials/markdowndemo)
