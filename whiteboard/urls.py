from django.conf.urls import patterns, url
from whiteboard.views import (
    create_task_page,
    developers_page,
    iterations_page,
    stories_page,
    story_page,
    task_page,
    write_page,
    )


urlpatterns = patterns(
    'whiteboard',
    url(r'developers/$', developers_page),
    url(r'iterations/$', iterations_page),
    url(r'queue/$', stories_page, name="queue"),
    url(r'queue/new/$', write_page),
    url(r'queue/(?P<story_id>\d+)/edit/$', write_page, name="edit_story"),
    url(r'queue/(?P<story_id>\d+)/$', story_page, name="story"),
    url(r'queue/(?P<story_id>\d+)/new/$',
        create_task_page,
        name="create_task"),
    url(r'queue/(?P<story_id>\d+)/task/(?P<task_id>\d+)/$',
        task_page,
        name="task"),
    url(r'queue/(?P<story_id>\d+)/task/(?P<task_id>\d+)/edit/$',
        create_task_page,
        name="edit_task"),
    )
