import json

from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404

from whiteboard.models import (
    Developer,
    Iteration,
    LoggedTime,
    Story,
    Task,
    convert_seconds,
    )


templ = lambda name: "whiteboard/{name}".format(name=name)


def JSONResponse(content={}, status=200, reason=None):
    """ Return HttpResponse as JSON response """
    return HttpResponse(json.dumps(content),
                        content_type="application/json",
                        status=status,
                        reason=reason)


def homepage(request):
    return stories_page(request)


def stories_page(request):
    if request.method == "POST":
        if "delete" in request.POST:
            for item in request.POST.getlist("selection", []):
                story_id = int(item)
                story = Story.objects.get(pk=story_id)
                story.delete()
                del story  # for flakes satisfaction
        return redirect("queue")

    stories = Story.objects.all()
    total_estimate = sum([story.estimate for story in stories])
    total_spent = sum([story.spent_time_seconds for story in stories])
    context = {"stories": stories}
    context["total_spent"] = convert_seconds(total_spent)
    context["total_estimate"] = convert_seconds(total_estimate)
    return render(request, templ("home.html"), context)


def story_page(request, story_id):
    story = get_object_or_404(Story, pk=story_id)
    context = {"story": story}
    if request.method == "POST":
        if "delete" in request.POST:
            for item in request.POST.getlist("selection", []):
                task_id = int(item)
                task = Task.objects.get(pk=task_id)
                task.delete()
        return redirect("story", story_id=story.pk)

    return render(request, templ("story.html"), context)


def write_page(request, story_id=0):
    story_id = int(story_id)
    context = {}
    story = None
    if story_id > 0:
        story = get_object_or_404(Story, pk=story_id)
    else:
        story = Story()

    if request.method == "POST":
        post = request.POST
        title = post.get("title", "").strip()
        content = post.get("content", "").strip()
        estimate = post.get("estimate", "").strip()
        story.estimate = estimate
        try:
            estimate = float(estimate)
        except (ValueError, TypeError):
            estimate = None
        story.title = title
        story.content = content
        if title and content and estimate:
            story.estimate = estimate * 3600
            story.save()
            return redirect("queue")
        else:
            context["errors"] = "Missing data"

    context["story"] = story
    return render(request, templ("write.html"), context)


def task_page(request, story_id, task_id):
    content = {}
    task = get_object_or_404(Task, pk=task_id)
    story = get_object_or_404(Story, pk=story_id)
    content["task"] = task
    content["story"] = story
    content["developers"] = Developer.objects.all()
    if request.method == "POST":
        try:
            logged_time = float(request.POST.get("time"))
        except (ValueError, TypeError):
            logged_time = None
        dev_id = request.POST.get("developer", 0)
        developer = Developer.objects.get(pk=dev_id)
        if developer and logged_time:
            logged = LoggedTime()
            logged.task = task
            logged.developer = developer
            logged.time = int(logged_time*60)
            logged.save()
            return redirect("task", story.pk, task.pk)

    return render(request, templ("task.html"), content)


def create_task_page(request, story_id, task_id=0):
    story = get_object_or_404(Story, pk=story_id)
    task = None
    if int(task_id) > 0:
        task = Task.objects.get(pk=task_id)
    else:
        task = Task()

    if request.method == "POST":
        post = request.POST
        title = post.get("title", "")
        content = post.get("content", "")
        try:
            estimate = float(post.get("estimate", ""))
            task.estimate = estimate * 3600
        except (ValueError, TypeError):
            estimate = None
        try:
            developer = Developer.objects.get(pk=int(post.get("developer", 0)))
        except Developer.DoesNotExist:
            developer = None
        try:
            iteration = Iteration.objects.get(pk=int(post.get("iteration", 0)))
        except Iteration.DoesNotExist:
            iteration = None
        task.title = title
        task.body = content
        if title and content and estimate and developer and iteration:
            task.developer = developer
            task.iteration = iteration
            task.story = story
            task.save()
            return redirect("story", story_id=story.id)

    content = {}
    content["developers"] = Developer.objects.all()
    content["iterations"] = Iteration.objects.all()
    content["task"] = task
    return render(request, templ("write_task.html"), content)


def developers_page(request):
    """ Return a list of developers or create a new one when
    POST'ed
    """
    context = {}
    if request.method == "POST":
        return create_developer(request)

    context["developers"] = Developer.objects.all()
    return render(request, templ("devs.html"), context)


def iterations_page(request):
    """ Return a list of iterations, or create a new one when
    POST'ed
    """
    context = {}
    if request.method == "POST":
        return create_iteration(request)
    context["iterations"] = Iteration.objects.all()
    return render(request, templ("iters.html"), context)


def extract_item(request, name):
    """ Extract POST or JSON data argument from POST'ed data """
    value = request.POST.get(name, "").strip()
    if not value:
        try:
            value = json.loads(request.body.decode()).get(name, "").strip()
            if not value:
                msg = "Couldn't extract value for name: {0}"
                raise ValueError(msg.format(name))
        except (ValueError, TypeError, AttributeError):
            raise
    return value


def create_developer(request):
    try:
        name = extract_item(request, "name")
        return create_item(Developer, "name", name)
    except (TypeError, ValueError, AttributeError):
        return JSONResponse({"msg": "Error parsing data"}, status=400)


def create_iteration(request):
    try:
        title = extract_item(request, "title")
        return create_item(Iteration, "title", title)
    except (TypeError, ValueError, AttributeError):
        return JSONResponse({"msg": "Error parsing data"}, status=400)


def create_item(klass, name, value):
    klass.objects.create(**{name: value})
    return JSONResponse({name: value}, status=201)
