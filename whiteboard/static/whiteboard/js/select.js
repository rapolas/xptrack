document.addEventListener('DOMContentLoaded', function(){

    // Select all checkbox functionality
    var select = document.getElementById("select-all");
    select.addEventListener("click", function(evt) {
        var elems = document.querySelectorAll("table input[type='checkbox']");
        for (var i = 0; i < elems.length; i++) {
            elems[i].checked = evt.target.checked;
        }
    });
    //
});
