document.addEventListener('DOMContentLoaded', function(){
    var submit = document.getElementById("btnCreate");
    submit.addEventListener("click", function(evt) {
        evt.preventDefault();
        var inpName = document.getElementById("inpName")
        var name = inpName.value.trim();
        if (!name) {
            inpName.style.backgroundColor = "rgb(255, 200, 200)";
            inpName.focus();
        } else {
            inpName.style.backgroundColor = "rgb(255, 255, 255)";
            inpName.value = "";
            var data = {"name": name};
            request = new XMLHttpRequest();
            request.open("POST", "");
            request.onload = function() {
                if (request.status >= 200 && request.status < 400) {
                    data = JSON.parse(request.responseText);
                    var list = document.getElementById("dev-list");
                    var ul = list.querySelector("ul");
                    if (ul === null) {
                        list.innerHTML = "";
                        ul = document.createElement("ul");
                        list.appendChild(ul);
                    }
                    var li = document.createElement("li");
                    li.appendChild(document.createTextNode(data.name));
                    ul.appendChild(li);
                } else {
                    // There was an error from the server
                }
            };
            request.onerror = function() {
                // We were not able to connect to server
            }
            var token = document.querySelectorAll("form input[name='csrfmiddlewaretoken']")[0].value;
            request.setRequestHeader("X-CSRFToken", token);
            request.send(JSON.stringify(data));
        }

        return false;
    });

});

