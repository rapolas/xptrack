document.addEventListener('DOMContentLoaded', function(){
    var submit = document.getElementById("btnCreate");
    submit.addEventListener("click", function(evt) {
        evt.preventDefault();
        var inpTitle = document.getElementById("inpTitle")
        var title = inpTitle.value.trim();
        if (!title) {
            inpTitle.style.backgroundColor = "rgb(255, 200, 200)";
            inpTitle.focus();
        } else {
            inpTitle.style.backgroundColor = "rgb(255, 255, 255)";
            inpTitle.value = "";
            var data = {"title": title};
            request = new XMLHttpRequest();
            request.open("POST", "");
            request.onload = function() {
                if (request.status >= 200 && request.status < 400) {
                    data = JSON.parse(request.responseText);
                    var list = document.getElementById("iter-list");
                    var ul = list.querySelector("ul");
                    if (ul === null) {
                        list.innerHTML = "";
                        ul = document.createElement("ul");
                        list.appendChild(ul);
                    }
                    var li = document.createElement("li");
                    li.appendChild(document.createTextNode(data.title));
                    ul.appendChild(li);
                } else {
                    // There was an error from the server
                }
            };
            request.onerror = function() {
                // We were not able to connect to server
            }
            var token = document.querySelectorAll("form input[name='csrfmiddlewaretoken']")[0].value;
            request.setRequestHeader("X-CSRFToken", token);
            request.send(JSON.stringify(data));
        }

        return false;
    });

});


