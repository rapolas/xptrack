import json

from django.core.urlresolvers import resolve, reverse
from django.http import HttpRequest, Http404
from django.http.request import QueryDict
from django.template.loader import render_to_string
from django.test import TestCase
from urllib.parse import urlencode

from whiteboard.views import (
    homepage, stories_page, story_page, write_page,
    task_page, create_task_page, developers_page, iterations_page)
from whiteboard.models import (
    Developer, Iteration, Story, Task, LoggedTime, convert_seconds)


class BaseTest(TestCase):
    """ Base class for our tests which should contain some helper
    methods used all over the tests
    """
    def create_story(self, title="First title",
                     content="Some content", estimate=900):
        return Story.objects.create(
            title=title,
            content=content,
            estimate=estimate,
            )

    def create_developer(self, name="Alfonso"):
        return Developer.objects.create(name=name)

    def create_iteration(self, title="Rev.1"):
        return Iteration.objects.create(title=title)

    def create_task(self, title="Some random title", body="Even more random",
                    estimate=3600, developer=None, iteration=None,
                    story=None):
        if developer is None:
            developer = self.create_developer()
        if iteration is None:
            iteration = self.create_iteration()
        if story is None:
            story = self.create_story()

        return Task.objects.create(
            title=title,
            body=body,
            estimate=estimate,
            developer=developer,
            iteration=iteration,
            story=story)

    def logtime(self, task, developer=None, time=600):
        if developer is None:
            developer = self.create_developer()
        LoggedTime.objects.create(task=task, developer=developer, time=time)


class HomePageTest(BaseTest):
    """ Home page ("/") tests """
    def test_root_url_points_to_homepage(self):
        """ Test that homepage function is correctly resolved """
        found = resolve('/')
        self.assertEqual(found.func, homepage)


class StoriesPageTest(BaseTest):
    """ Stories page tests """

    def test_stories_page_is_correct(self):
        """ Test that stories page is correctly rendered """
        response = self.client.get('/whiteboard/queue/')

        stories = Story.objects.all()
        total_estimate = sum([story.estimate for story in stories])
        total_spent = sum([story.spent_time_seconds for story in stories])
        context = {"stories": stories}
        context["total_spent"] = convert_seconds(total_spent)
        context["total_estimate"] = convert_seconds(total_estimate)

        expected = render_to_string("whiteboard/home.html", context)

        self.assertEqual(response.content.decode(), expected)

    def test_story_can_be_deleted_in_stories_page(self):
        story = self.create_story("First story title")
        self.assertEqual(Story.objects.count(), 1)

        request = HttpRequest()
        response = stories_page(request)
        self.assertContains(response, "First story title")

        request = HttpRequest()
        request.method = "POST"
        data = {"delete": "Delete", "selection": [story.pk]}
        request.POST = QueryDict(urlencode(data, doseq=True))

        response = stories_page(request)
        self.assertEqual(Story.objects.count(), 0)

    def test_stories_page_redirect_after_post(self):
        request = HttpRequest()
        request.method = "POST"
        response = stories_page(request)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response["location"], reverse("queue"))


class StoryPageTest(BaseTest):

    def test_non_existing_story_page_is_404(self):
        request = HttpRequest()
        self.assertRaises(Http404, story_page, request, story_id=1)

    def test_correct_page_rendered(self):
        story = self.create_story()
        request = HttpRequest()
        response = story_page(request, story.pk)
        expected = render_to_string("whiteboard/story.html", {"story": story})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content.decode(), expected)

    def test_story_title_is_in_page(self):
        story = self.create_story(title="Super story")
        request = HttpRequest()
        response = story_page(request, story.pk)

        self.assertContains(response, story.title)

    def test_story_page_can_delete_task(self):
        story = self.create_story()
        task = self.create_task(story=story)
        request = HttpRequest()
        request.method = "POST"
        data = {"delete": "Delete", "selection": [task.pk]}
        request.POST = QueryDict(urlencode(data, doseq=True))
        response = story_page(request, story.pk)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(story.task_set.count(), 0)

    def test_story_page_shows_tasks(self):
        story = self.create_story()
        self.create_task(title="Title one", story=story)
        self.create_task(title="Title two", story=story)
        request = HttpRequest()
        response = story_page(request, story.pk)
        self.assertContains(response, "Title one")
        self.assertContains(response, "Title two")


class StoryWritePageTest(BaseTest):
    def make_post_request(self, title="", content="", estimate="0"):
        request = HttpRequest()
        request.method = "POST"
        request.POST["title"] = title
        request.POST["content"] = content
        request.POST["estimate"] = estimate
        return request

    def test_correct_html_returned(self):
        request = HttpRequest()
        response = write_page(request)
        expected = render_to_string("whiteboard/write.html")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content.decode(), expected)

    def test_submit_story_works(self):
        request = self.make_post_request("Some title", "Some content", "0.25")

        response = write_page(request)

        self.assertEqual(response.status_code, 302)
        story = Story.objects.all()[0]
        self.assertEqual(Story.objects.count(), 1)
        self.assertEqual(story.title, "Some title")
        self.assertEqual(story.estimate, 900)

    def test_edit_story_works(self):
        story = self.create_story(title="Some title", content="Some content")
        request = self.make_post_request(
            "Another title", story.content, "0.25")

        response = write_page(request, story_id=story.pk)

        self.assertEqual(response.status_code, 302)
        story = Story.objects.all()[0]
        self.assertEqual(Story.objects.count(), 1)
        self.assertEqual(story.title, "Another title")

    def test_fields_are_filled_on_edit(self):
        story = self.create_story(title="Some title",
                                  content="Some content",
                                  estimate=900)
        request = HttpRequest()
        response = write_page(request, story_id=story.pk)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Some title")
        self.assertContains(response, "Some content")
        self.assertContains(response, "0.25")

    def test_post_is_not_accepting_invalid_values(self):
        request = self.make_post_request(
            "Some title", "Some content", "error")

        response = write_page(request)

        self.assertEqual(Story.objects.count(), 0)
        self.assertContains(response, "Missing data")

    def test_values_stay_after_unsuccessful_post(self):
        request = self.make_post_request(
            "Some title", "Some content", "error")
        response = write_page(request)
        self.assertContains(response, "Some title")
        self.assertContains(response, "Some content")


class TaskPageTest(BaseTest):

    def test_correct_html_returned(self):
        request = HttpRequest()
        story = self.create_story()
        task = self.create_task()
        response = task_page(request, story_id=story.pk, task_id=task.pk)
        expected = render_to_string(
            "whiteboard/task.html",
            {"story": story,
             "task": task,
             "developers": Developer.objects.all()})

        self.assertEqual(response.content.decode(), expected)

    def test_no_story_raises_404(self):
        request = HttpRequest()
        self.assertRaises(Http404, task_page, request, story_id=1, task_id=1)

    def test_no_task_raises_404(self):
        story = self.create_story()
        self.assertRaises(
            Http404, task_page, HttpRequest(), story_id=story.pk, task_id=1)

    def test_task_page_shows_task(self):
        story = self.create_story()
        developer = self.create_developer()
        iteration = self.create_iteration()

        task = self.create_task(title="Some title",
                                body="Some interesting task",
                                estimate=900,
                                story=story,
                                developer=developer,
                                iteration=iteration
                                )
        response = task_page(HttpRequest(), story_id=story.pk, task_id=task.pk)
        self.assertContains(response, "Some title")
        self.assertContains(response, "Some interesting task")
        self.assertContains(response, "0h 15m")

    def test_task_page_redirects_after_post(self):
        story = self.create_story()
        developer = self.create_developer()
        task = self.create_task()
        request = HttpRequest()
        request.method = "POST"
        request.POST["developer"] = developer.pk
        request.POST["time"] = 15
        response = task_page(request, story_id=story.pk, task_id=task.pk)
        self.assertEqual(response.status_code, 302)

    def test_task_page_accepts_post(self):
        story = self.create_story()
        developer = self.create_developer()
        task = self.create_task(story=story, developer=developer)

        request = HttpRequest()
        request.method = "POST"
        request.POST["time"] = 15  # minutes
        request.POST["developer"] = developer.pk
        response = task_page(request, story_id=story.pk, task_id=task.pk)

        updated = Task.objects.get(pk=task.pk)
        self.assertEqual(updated.spent_time, convert_seconds(15*60))
        response = task_page(HttpRequest(), story_id=story.pk, task_id=task.pk)
        self.assertContains(response, "0h 15m")

    def test_task_page_doesnt_accept_invalid_value(self):
        story = self.create_story()
        developer = self.create_developer()
        task = self.create_task(story=story, developer=developer)

        request = HttpRequest()
        request.method = "POST"
        request.POST["time"] = "aaa"
        request.POST["developer"] = developer.pk
        response = task_page(request, story_id=story.pk, task_id=task.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(LoggedTime.objects.count(), 0)


class CreateTaskPage(BaseTest):
    def make_post_request(self, title="", content="", estimate="0",
                          developer=None, iteration=None):
        if developer is None:
            developer = self.create_developer()
        if iteration is None:
            iteration = self.create_iteration()

        request = HttpRequest()
        request.method = "POST"
        request.POST["title"] = title
        request.POST["content"] = content
        request.POST["estimate"] = estimate
        request.POST["developer"] = developer.pk
        request.POST["iteration"] = iteration.pk
        return request

    def test_correct_html_returned(self):
        story = self.create_story()
        developers = Developer.objects.all()
        response = create_task_page(HttpRequest(), story_id=story.pk)
        data = {"developers": developers}
        data["iterations"] = Iteration.objects.all()

        expected = render_to_string("whiteboard/write_task.html", data)
        self.assertEqual(response.content.decode(), expected)

    def test_submit_task_works(self):
        story = self.create_story()
        request = self.make_post_request("Some title", "Some content", "0.25")
        response = create_task_page(request, story_id=story.pk)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Task.objects.count(), 1)

    def test_submit_with_missing_data_doesnt_proceed(self):
        story = self.create_story()
        request = self.make_post_request()
        response = create_task_page(request, story_id=story.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Task.objects.count(), 0)

    def test_submit_invalid_estimate(self):
        story = self.create_story()
        request = self.make_post_request(estimate="error")
        response = create_task_page(request, story_id=story.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Task.objects.count(), 0)

    def test_submit_invalid_data_keeps_existing_data(self):
        story = self.create_story()
        request = self.make_post_request(title="Some content")
        response = create_task_page(request, story.pk)
        self.assertContains(response, "Some content")

    def test_editing_task_populates_fields(self):
        story = self.create_story()
        task = self.create_task(title="Some title",
                                body="Some content",
                                story=story)
        response = create_task_page(HttpRequest(), story.pk, task.pk)
        self.assertContains(response, "Some title")
        self.assertContains(response, "Some content")

    def test_editing_updates_task(self):
        story = self.create_story()
        task = self.create_task(title="Some title",
                                body="Some content",
                                story=story)
        request = self.make_post_request(
            title="Changed title",
            content=task.body,
            estimate=task.estimate/3600)
        response = create_task_page(request, story.pk, task.pk)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Task.objects.get(pk=task.pk).title, "Changed title")


class DeveloperTest(BaseTest):
    def test_correct_html_returned(self):
        response = developers_page(HttpRequest())
        expected = render_to_string("whiteboard/devs.html")
        self.assertEqual(response.content.decode(), expected)

    def test_posting_developer_works(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST["name"] = "John"
        response = developers_page(request)
        data = json.loads(response.content.decode())
        self.assertEqual({"name": "John"}, data)
        self.assertEqual(Developer.objects.count(), 1)

    def test_posting_invalid_doesnt_work(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST["n"] = "something"
        response = developers_page(request)
        data = json.loads(response.content.decode())
        self.assertEqual(data['msg'], 'Error parsing data')
        self.assertEqual(Developer.objects.count(), 0)

    def test_posting_json_data_works(self):
        Dummy = type("HttpRequest", (), {})
        request = Dummy()
        request.body = bytes(json.dumps({"name": "John"}), "utf-8")
        request.method = "POST"
        request.content_type = "application/json"
        request.POST = QueryDict({})

        response = developers_page(request)
        data = json.loads(response.content.decode())
        self.assertEqual(data, {"name": "John"})

    def test_posting_invalid_json_data(self):
        Dummy = type("HttpRequest", (), {})
        request = Dummy()
        request.body = bytes(json.dumps({}), "utf-8")
        request.method = "POST"
        request.content_type = "application/json"
        request.POST = QueryDict({})

        response = developers_page(request)
        data = json.loads(response.content.decode())
        self.assertIn("msg", data)
        self.assertEqual(data["msg"], "Error parsing data")


class IterationTest(BaseTest):
    def test_correct_html_returned(self):
        response = iterations_page(HttpRequest())
        expected = render_to_string("whiteboard/iters.html")
        self.assertEqual(response.content.decode(), expected)

    def test_posting_iteration_works(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST["title"] = "Rev.1"
        response = iterations_page(request)
        data = json.loads(response.content.decode())
        self.assertEqual({"title": "Rev.1"}, data)
        self.assertEqual(Iteration.objects.count(), 1)

    def test_posting_invalid_doesnt_work(self):
        request = HttpRequest()
        request.method = "POST"
        response = iterations_page(request)
        data = json.loads(response.content.decode())
        self.assertEqual(data["msg"], "Error parsing data")
        self.assertEqual(Developer.objects.count(), 0)

    def test_posting_json_data_works(self):
        Dummy = type("HttpRequest", (), {})
        request = Dummy()
        request.body = bytes(json.dumps({"title": "Rev.1"}), "utf-8")
        request.method = "POST"
        request.content_type = "application/json"
        request.POST = QueryDict({})

        response = iterations_page(request)
        data = json.loads(response.content.decode())
        self.assertEqual(data, {"title": "Rev.1"})

    def test_posting_invalid_json_data(self):
        Dummy = type("HttpRequest", (), {})
        request = Dummy()
        request.body = bytes(json.dumps({}), "utf-8")
        request.method = "POST"
        request.content_type = "application/json"
        request.POST = QueryDict({})

        response = iterations_page(request)
        data = json.loads(response.content.decode())
        self.assertIn("msg", data)
        self.assertEqual(data["msg"], "Error parsing data")


class StoryModelTest(BaseTest):

    def test_seconds_converted(self):
        """ Test whether our second convert works """
        self.assertEqual(convert_seconds(900), "0h 15m")
        self.assertEqual(convert_seconds(3600), "1h 0m")

    def test_creating_and_retrieving(self):
        first_story = self.create_story(
            "First story title",
            "And here comes the content of user story",
            0.25)

        second_story = self.create_story(
            "First story title",
            "And here comes the content of user story",
            0.5)

        saved_items = Story.objects.all()
        self.assertEqual(saved_items.count(), 2)
        first = saved_items[0]
        second = saved_items[1]
        self.assertEqual(first, first_story)
        self.assertEqual(second, second_story)

    def test_estimate_time_correctly_shown(self):
        story = self.create_story(estimate=900)
        saved = Story.objects.get(pk=story.pk)
        self.assertEqual(saved.estimate_time, convert_seconds(900))

    def test_spent_time_correctly_shown(self):
        task = self.create_task()
        self.logtime(task, time=1800)
        self.assertEqual(Story.objects.count(), 1)
        saved = Story.objects.get(pk=task.story.pk)
        self.assertEqual(saved.spent_time, convert_seconds(1800))
        self.assertEqual(saved.spent_time_seconds, 1800)

    def test_estimate_hours_is_correctly_returned(self):
        story = self.create_story(estimate=483.732)
        saved = Story.objects.get(pk=story.pk)
        self.assertEqual(
            "{0:.3f}".format(saved.estimate_hours),
            "{0:.3f}".format(0.13437))

    def test_assigned_time_is_counted_correctly(self):
        story = self.create_story()
        self.create_task(story=story, estimate=300)
        self.create_task(story=story, estimate=900)
        self.assertEqual(story.task_assigned_time, convert_seconds(300+900))


class DeveloperModelTest(BaseTest):
    def test_developer_can_be_saved(self):
        dev = self.create_developer(name="John")
        saved = Developer.objects.get(pk=dev.pk)
        self.assertEqual(dev.name, saved.name)


class IterationModelTest(BaseTest):
    def test_iteration_can_be_saved(self):
        iteration = self.create_iteration(title="Rev.1")
        saved = Iteration.objects.get(pk=iteration.pk)
        self.assertEqual(iteration.title, saved.title)


class TaskModelTest(BaseTest):
    def test_task_can_be_saved(self):
        story = self.create_story()
        developer = self.create_developer()
        iteration = self.create_iteration()
        task = self.create_task(title="Random Title",
                                body="Even more random body",
                                estimate=3600,
                                developer=developer,
                                iteration=iteration,
                                story=story)
        saved = Task.objects.get(pk=task.pk)
        self.assertEqual(saved.title, "Random Title")
        self.assertEqual(saved.body, "Even more random body")
        self.assertEqual(saved.estimate, 3600)
        self.assertEqual(saved.developer.name, developer.name)
        self.assertEqual(saved.iteration.title, iteration.title)
        self.assertEqual(saved.story.pk, story.pk)

    def test_spent_time_is_correctly_shown(self):
        task = self.create_task()
        self.logtime(task, time=3600)
        self.logtime(task, time=3600)
        self.assertEqual(task.spent_time, convert_seconds(3600+3600))

    def test_estimate_time_is_correctly_shown(self):
        task = self.create_task(estimate=900)
        self.assertEqual(task.estimate_time, convert_seconds(900))

    def test_estimate_hours_is_correctly_returned(self):
        task = self.create_task(estimate=1800)
        self.assertEqual(task.estimate_hours, 0.5)
