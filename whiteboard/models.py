from django.db import models


def convert_seconds(seconds):
    """ Convert given seconds to Xh Ym format """
    minutes = int(seconds / 60)
    fmt = "{h}h {m}m"
    return fmt.format(h=int(minutes/60), m=minutes % 60)


class Story(models.Model):
    title = models.CharField(max_length=1000)
    content = models.TextField()
    estimate = models.IntegerField()  # estimated time converted to seconds

    @property
    def estimate_time(self):
        """ Convert given time to more friendly format HHh MMm
        (e.g. 3h 0m)
        """
        return convert_seconds(self.estimate)

    @property
    def spent_time(self):
        """ Return total spent time in child tasks for this story """
        return convert_seconds(self.spent_time_seconds)

    @property
    def estimate_hours(self):
        """ Convert seconds back to hours and show as float """
        return self.estimate / 3600

    @property
    def spent_time_seconds(self):
        """ Return spent time on all tasks associated with the story
        in seconds
        """
        spent = 0
        for task in self.task_set.all():
            for logged in task.loggedtime_set.all():
                spent += logged.time
        return spent

    @property
    def task_assigned_time(self):
        """ Return accumulated time estimated on child tasks """
        estimate = 0
        for task in self.task_set.all():
            estimate += task.estimate
        return convert_seconds(estimate)


class Developer(models.Model):
    name = models.CharField(max_length=30)


class Iteration(models.Model):
    title = models.CharField(max_length=100)


class Task(models.Model):
    title = models.CharField(max_length=1000)
    body = models.TextField()
    estimate = models.IntegerField()  # estimate time converted to seconds
    developer = models.ForeignKey(Developer)
    iteration = models.ForeignKey(Iteration)
    story = models.ForeignKey(Story)

    @property
    def spent_time(self):
        """ Return sum of spent time in HHh MMm (e.g. 3h 15m) format """
        spent = 0
        for logged in self.loggedtime_set.all():
            spent += logged.time
        return convert_seconds(spent)

    @property
    def estimate_time(self):
        """ Return estimate time in HHh MMm (e.g. 3h 15m) format """
        return convert_seconds(self.estimate)

    @property
    def estimate_hours(self):
        """ Return estimate in hours """
        return self.estimate / 3600


class LoggedTime(models.Model):
    """ Keeps track of logged time for task for each developer """
    developer = models.ForeignKey(Developer)
    task = models.ForeignKey(Task)
    time = models.IntegerField()  # spent time in seconds
