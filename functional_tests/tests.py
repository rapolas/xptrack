from django.contrib.staticfiles.testing import StaticLiveServerCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class TrackerVisitorTest(StaticLiveServerCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_start_using_page(self):
        browser = self.browser

        # John opens a browser to checkout its homepage
        browser.get(self.live_server_url)

        # He sees the page title mentions "XPTracker" and
        # there is a header for existing stories
        self.assertIn("XPTracker", browser.title)
        header_text = browser.find_element_by_tag_name("h1").text
        self.assertIn("Existing Stories", header_text)

        # There are no stories, so message tells us that
        self.assertIn("There are no stories so far", browser.page_source)
        self.assertNotIn("Title Spent/Estimate", browser.page_source)

        # There are also three links: Developers Iterations and Stories/Tasks
        links = browser.find_elements_by_tag_name("a")
        links_texts = [link.text for link in links]
        self.assertIn("Developers", links_texts)
        self.assertIn("Iterations", links_texts)
        self.assertIn("Stories/Tasks", links_texts)

        # Before using stories/tasks, at least one developer and
        # one iteration should be added

        # Lets add couple of developers
        devs_link = browser.find_element_by_link_text("Developers")
        devs_link.click()

        inputbox = browser.find_element_by_name("name")
        self.assertEqual(inputbox.get_attribute("placeholder"),
                         "Enter developer name here")
        inputbox.send_keys("Sarah")
        inputbox.send_keys(Keys.ENTER)
        items = self.browser.find_elements_by_tag_name("li")
        self.assertTrue(any(item.text == "Sarah" for item in items))
        inputbox.send_keys("Steven")
        inputbox.send_keys(Keys.ENTER)
        items = self.browser.find_elements_by_tag_name("li")
        self.assertTrue(any(item.text == "Steven" for item in items))

        # And an iteration
        iters_link = browser.find_element_by_link_text("Iterations")
        iters_link.click()

        message = browser.find_element_by_id("iter-list")
        self.assertEqual(message.text, "No iterations exist on the system")
        inputbox = browser.find_element_by_name("title")
        self.assertEqual(inputbox.get_attribute("placeholder"),
                         "Enter iteration title here")
        inputbox.send_keys("ver1.1")
        inputbox.send_keys(Keys.ENTER)
        items = self.browser.find_elements_by_tag_name("li")
        self.assertIn("ver1.1", [item.text for item in items])
        # message that there are no iterations is gone
        message = browser.find_element_by_id("iter-list")
        self.assertNotEqual(message.text, "No iterations exist on the system")

        # Once we have developer(s) and iteration(s) we can add a story
        browser.find_element_by_link_text("Stories/Tasks").click()
        browser.find_element_by_link_text("Create a story").click()
        browser.find_element_by_id("title").send_keys(
            "Example story",
            Keys.TAB,
            "User comes here and fills a story about expected behaviour",
            Keys.TAB,
            "4",
            Keys.ENTER
            )

        # story is created lets check it out
        browser.find_element_by_link_text("Example story").click()
        message = browser.find_element_by_id("story")
        self.assertIn("Example story", message.text)
        self.assertIn("Spent/Estimate", message.text)
        self.assertIn("4h 0m", message.text)

        # There are no tasks filled
        self.assertIn("There are no tasks for this story yet.",
                      browser.page_source)
        self.assertNotIn("Title Spent/Estimate", browser.page_source)

        # lets add a task
        browser.find_element_by_link_text("Add task").click()
        browser.find_element_by_id("title").send_keys(
            "Lorem ipsum",
            Keys.TAB,
            "Donec nec justo eget felis facilisis fermentum.")
        select = browser.find_element_by_id("developer")
        for option in select.find_elements_by_tag_name("option"):
            if option.text == "Steven":
                option.click()
                break

        select = browser.find_element_by_id("iteration")
        for option in select.find_elements_by_tag_name("option"):
            if option.text == "ver1.1":
                option.click()
                break
        browser.find_element_by_id("estimate").send_keys("2", Keys.ENTER)

        # task has been created, lets see if it is visible in task view
        message = browser.find_element_by_id("story")
        self.assertIn("Lorem ipsum", message.text)

        # lets log some spent time to a task
        browser.find_element_by_link_text("Lorem ipsum").click()
        form = browser.find_element_by_tag_name("form")
        for option in form.find_elements_by_tag_name("option"):
            if option.text == "Steven":
                option.click()
                break
        browser.find_element_by_name("time").send_keys(60, Keys.ENTER)
        self.assertIn("1h 0m / 2h 0m",
                      browser.find_element_by_id("story").text)

        # lets log more time
        form = browser.find_element_by_tag_name("form")
        for option in form.find_elements_by_tag_name("option"):
            if option.text == "Steven":
                option.click()
                break
        browser.find_element_by_name("time").send_keys(60, Keys.ENTER)
        self.assertIn("2h 0m / 2h 0m",
                      browser.find_element_by_id("story").text)

        # Oh crap, title is wrong, need to fix it
        browser.find_element_by_link_text("Edit").click()
        browser.find_element_by_id("title").clear()
        browser.find_element_by_id("title").send_keys(
            "Finish all the tests", Keys.ENTER)

        div = browser.find_element_by_id("story")
        self.assertIn("Finish all the tests", div.text)

        # Task used all the time, hopefully all done - remove it
        browser.find_element_by_id("select-all").click()
        browser.find_element_by_css_selector("td input[name='delete']").click()
        div = browser.find_element_by_id("story")
        self.assertNotIn("Finish all the tests", div.text)

        # Don't care about the user story either - delete it
        browser.find_element_by_name("delete").click()
        div = browser.find_element_by_id("content")
        self.assertNotIn("Example story", div.text)
